from django.apps import AppConfig


class FechamentosConfig(AppConfig):
    name = 'fechamentos'
