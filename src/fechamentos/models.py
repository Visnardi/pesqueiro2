from django.db import models

# Create your models here.

class Pagamento(models.Model):

	dinheiro = 1
	debito = 2
	credito = 3
	furo = 4

	escolhas_pagamento = (
			(dinheiro, "Dinheiro"),
			(debito, "Debito"),
			(credito, "Credito"),
			(furo, "Furo"),
		)

	tipos_pagamento = {
		dinheiro:"Dinheiro",
		debito:"Debito",
		credito:"Credito",
		furo:"Furo",
	}

	id_pagamento = models.AutoField(primary_key = True)
	tipo_pagamento = models.PositiveIntegerField(choices = escolhas_pagamento)
	valor = models.DecimalField(max_digits=8, decimal_places=2)
	data_pagamento = models.DateTimeField(auto_now=True, auto_now_add=False)
	deletado = models.BooleanField(default=0)
	id_comandas = models.CharField(max_length=200)

	
	def __str__(self):
		return str(self.id_pagamento)

	def computa_pgtos(self):
		valor = self.valor
		tipo_pagamento = self.tipos_pagamento[self.tipo_pagamento]
		return valor, tipo_pagamento

