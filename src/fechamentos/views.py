from decimal import *

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.generic import View
from django.shortcuts import render

from comandas.models import Comanda
from pesagens.models import Pesagem
from compras.models import Compra

from .forms import PagamentoForm1, PagamentoForm2
from .models import Pagamento


# Create your views here.

class FechaComandas(View):

	def get(self, request, *args, **kwargs):
		context = {
			'aux_var':0,

		}
		return render(request, 'fecha_comandas.html', context)

	def post(self, request, *args, **kwargs):

		if "busca" in request.POST:
			num_comandas = request.POST["num_comandas"].split(";")
			comandas = Comanda.objects.filter(num_comanda__in=num_comandas).filter(status=1)
			id_comandas = comandas.values("id_comanda")
			#PESAGENS
			pesagens1 = Pesagem.objects.filter(id_comanda__in=comandas).filter(deletado=0).order_by("peixe")
			pesagens = {}
			total_pesagens, total_kg = 0, 0
			for i, v  in enumerate(pesagens1):
				tipo_peixe, tipo_limpesa, valor_pesagem = v.computa_pesagem()
				pesagens[i + 1] = {
					'tipo_peixe':tipo_peixe,
					'peso':v.peso,
					'tipo_limpesa':tipo_limpesa,
					'valor_pesagem':valor_pesagem,
					'id_pesagem':v.id_pesagem,
				}
				total_kg += v.peso
				total_pesagens += valor_pesagem
			#COMPRAS
			compras = Compra.objects.filter(id_comanda__in=comandas).filter(deletado=0).order_by("id_produto")
			total_compras = 0
			for i in compras:
				total_compras += i.valor
			#ENTRADAS
			qtd_pkg, valor_pkg, qtd_esp, valor_esp, qtd_inf, valor_inf, total_entradas = 0, 0, 0, 0, 0, 0, 0
			for i in comandas:
				tipo_entrada, valor_entrada_Adulto, valor_entrada_Infantil, subtotal_entrada = i.computa_entradas()
				if tipo_entrada == "Por Kg":
					qtd_pkg += 1
					valor_pkg += valor_entrada_Adulto
				elif tipo_entrada == "Esportiva":
					qtd_esp += 1
					valor_esp += valor_entrada_Adulto
				qtd_inf += i.entradas_infantil
				valor_inf += valor_entrada_Infantil
				total_entradas += subtotal_entrada

			total = Decimal(total_pesagens) + Decimal(total_compras) + Decimal(total_entradas)
			context={
				'inicial':request.POST["num_comandas"], 'aux_var':1,
				'total':total, 'pesagens':pesagens, 'total_kg':total_kg, 'total_pesagens':total_pesagens,
				'compras':compras, 'total_compras':total_compras, 'qtd_pkg':qtd_pkg, 'valor_pkg':valor_pkg,
				'qtd_esp':qtd_esp, 'valor_esp':valor_esp, 'qtd_inf':qtd_inf, 'valor_inf':valor_inf,
				'total_entradas':total_entradas,
			}

			return render(request, 'fecha_comandas.html', context)

#funcoes auxiliares de FechaComandas2

def calcula_restante(total, pgtos):
	restante = Decimal(total)
	for pgto in pgtos:
		restante -= pgto.valor
	return restante

def limpa_pagamentos(pagamentos):
	pgtos = {}
	for i, pgto in enumerate(pagamentos):
		valor, tipo_pagamento = pgto.computa_pgtos()
		pgtos[i] = {
			'id_pagamento':pgto.id_pagamento,
			'valor':valor,
			'tipo_pgto':tipo_pagamento
		}
		print(pgtos)
	return pgtos

class FechaComandas2(View):
	def post(self, request, *args, **kwargs):
		form_fechamento = PagamentoForm1()

		#identifica lista de comandas
		num_comandas = request.POST["num_comandas"].split(";")
		comandas = Comanda.objects.filter(num_comanda__in=num_comandas).filter(status=1)
		#monta string id_comandas
		id_comandas = ";".join([str(i['id_comanda']) for i in comandas.values("id_comanda")])
		pagamentos = Pagamento.objects.filter(id_comandas=id_comandas).filter(deletado=0)
	
		context = {
			'form_fechamento':form_fechamento,
			'total':request.POST['input_total'],
			'num_comandas':request.POST['num_comandas'],
			}

		if 'fechar' in request.POST:
			context['restante'] = calcula_restante(request.POST['input_total'], pagamentos)
			context['pgtos'] = limpa_pagamentos(pagamentos)
			return render(request, 'fecha_comandas2.html', context)

		if 'add_pgto' in request.POST:
			pgto_aux = request.POST.copy()
			pgto_aux['id_comandas'] = id_comandas
			pgto = PagamentoForm2(pgto_aux)
			if pgto.is_valid():
				pgto.save()
				# context['restante'] = Decimal pgto['valor']

		if 'deleta' in request.POST:
			pgto_del = Pagamento.objects.get(id_pagamento=request.POST['id_pgto'])
			pgto_del.deletado = 1
			pgto_del.save()

		if 'encerrar' in request.POST:
			for comanda in comandas:
				comanda.encerra_comanda()
			return HttpResponseRedirect(reverse('lista', kwargs={'quais': 'ativas'}))

		pgtos = Pagamento.objects.filter(id_comandas=id_comandas).filter(deletado=0)
		context['pgtos'] = limpa_pagamentos(pagamentos)
		context['restante'] = calcula_restante(request.POST['input_total'], pgtos)
		return render(request, 'fecha_comandas2.html', context)