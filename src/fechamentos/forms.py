from django import forms
from .models import Pagamento


class PagamentoForm1(forms.ModelForm):
	class Meta:
		model = Pagamento
		fields = [
			'tipo_pagamento',
			'valor',
			]

class PagamentoForm2(forms.ModelForm):
	class Meta:
		model = Pagamento
		fields =[
			'tipo_pagamento',
			'valor',
			'id_comandas',
		]