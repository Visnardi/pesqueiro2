from django import forms
from .models import Comanda

class ComandaForm1(forms.ModelForm):
	class Meta:
		model = Comanda
		fields =[
			'num_comanda',
			'id_cliente',
			'entrada_adulto',
			'entradas_infantil',
		]

	def __init__(self, *args, **kwargs):
		super(ComandaForm1, self).__init__(*args, **kwargs)
		self.fields['num_comanda'].widget.attrs['style'] = "width:200px"
		self.fields['id_cliente'].widget.attrs['style'] = "width:200px"
		self.fields['entrada_adulto'].widget.attrs['style'] = "width:200px"
		self.fields['entradas_infantil'].widget.attrs['style'] = "width:200px"

class ComandaForm2(forms.ModelForm):
	class Meta:
		model = Comanda
		fields =[
			'num_comanda',
			'id_cliente',
			'entrada_adulto',
			'entradas_infantil',
			'valor',
		]