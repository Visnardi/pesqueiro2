from decimal import *

from .models import Comanda
from .forms import ComandaForm1, ComandaForm2

from fechamentos.models import Pagamento
from fechamentos.forms import PagamentoForm1, PagamentoForm2

from pesagens.models import Pesagem
from pesagens.forms import PesagemForm1, PesagemForm2

from produtos.models import Produto
from compras.models import Compra
from compras.forms import CompraForm1, CompraForm2

from django.contrib import messages
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import IntegrityError
from django.http import HttpResponseRedirect, Http404
from django.views.generic import View
from django.shortcuts import render

# Create your views here.

class ListaComandas(View):
	def get(self, request, *args, **kwargs):
		if kwargs['quais'] == 'ativas':
			lista_comandas = Comanda.objects.filter(status=1).order_by('num_comanda')
			bool_todas = 0
			title = "Comandas Ativas"
		elif kwargs['quais'] == 'todas':
			lista_comandas = Comanda.objects.all().order_by('-data_comanda')
			bool_todas = 1
			title = "Todas Comandas"
		else:
			raise Http404("Bad server, no donuts for you.")
		paginator = Paginator(lista_comandas, 10) # Show 25 contacts per page
		page = request.GET.get('page')
		try:
			comandas = paginator.page(page)
		except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			comandas = paginator.page(1)
		except EmptyPage:
			# If page is out of range (e.g. 9999), deliver last page of results.
			comandas = paginator.page(paginator.num_pages)
		context = {
			'title':title,
			'bool_todas':bool_todas,
			'comandas':comandas
		}
		return render(request, 'lista_comandas.html', context)


class DetalheComanda(View):
	def get(self, request, *args, **kwargs):
		"""
			Buscar comanda, dados relativos a entradas, pesagens e compras
		"""
		comanda = Comanda.objects.get(id_comanda=kwargs['id_comanda'])
		#Dados relativos a entradas		
		tipo_entrada, valor_entrada_Adulto, valor_entrada_Infantil, subtotal_entrada = comanda.computa_entradas()
		#Dados relativos a pesagens
		pesagens1 = Pesagem.objects.filter(id_comanda=comanda.id_comanda).filter(deletado=0)
		pesagens = {}
		total_pesagens, total_kg = 0, 0
		for i, v  in enumerate(pesagens1):
			tipo_peixe, tipo_limpesa, valor_pesagem = v.computa_pesagem()
			pesagens[i + 1] = {
				'tipo_peixe':tipo_peixe,
				'peso':v.peso,
				'tipo_limpesa':tipo_limpesa,
				'valor_pesagem':valor_pesagem,
				'id_pesagem':v.id_pesagem,
			}
			total_kg += v.peso
			total_pesagens += valor_pesagem
		#Dados relativos a compras
		compras1 = Compra.objects.filter(id_comanda=comanda.id_comanda).filter(deletado=0)
		total_compras = 0
		for i in compras1:
			total_compras += i.valor

		#dados de contexto
		form_pesagens = PesagemForm1()
		form_compras = CompraForm1()
		total = Decimal(subtotal_entrada) +	Decimal(total_pesagens) + Decimal(total_compras)
		context = {
			'comanda':comanda,
			'total':total,
			'tipo_entrada':tipo_entrada,
			'valor_entrada_Adulto':valor_entrada_Adulto,
			'valor_entrada_Infantil':valor_entrada_Infantil,
			'subtotal_entrada':subtotal_entrada,
			'pesagens':pesagens,
			'total_kg':total_kg,
			'total_pesagens':total_pesagens,
			'form_pesagem':form_pesagens,
			'compras':compras1,
			'total_compras':total_compras,
			'form_compras':form_compras,
		}

		return render(request, 'detalhe_comanda.html', context)

	def post(self, request, *args, **kwargs):
		"""
			Adiciona pesagem/compra a comanda em questao
		"""
		comanda = Comanda.objects.get(id_comanda=kwargs['id_comanda'])
		if "add_pesagem" in request.POST:	
			form = PesagemForm1(request.POST)
			if form.is_valid():
				inst = request.POST.copy()
				valor = round(Decimal(inst['peso']) * (
					Decimal(Pesagem.valores_peixes[float(inst['peixe'])]) +
					Decimal(Pesagem.valores_limpesa[float(inst['limpesa'])])), 2)
				inst['valor'] = valor
				inst['id_comanda'] = comanda.id_comanda
				form2 = PesagemForm2(inst)
		elif "add_compra" in request.POST:
			form = CompraForm1(request.POST)
			if form.is_valid():
				inst = request.POST.copy()
				valor = Decimal(inst['qtd']) * Decimal(Produto.objects.get(id_produto=inst['id_produto']).valor)
				inst['valor'] = valor
				inst['id_comanda'] = comanda.id_comanda
				form2 = CompraForm2(inst)
		else:
			return HttpResponseRedirect(comanda.get_absolute_url())
		try:
			form2.save()
		except:
			print(form2)
			messages.error(request, 'ERRRRROUUU')

		return HttpResponseRedirect(comanda.get_absolute_url())

class deleta(View):
	def post(self, request, *args, **kwargs):
		"""
			**kwargs = {
			id_comanda = comanda para qual voltar depois da operacao,
			oque = tipo do objeto a ser deletado,
			id_doque = id do objeto a ser deletado
			}
		"""
		comanda = Comanda.objects.get(id_comanda=kwargs['id_comanda'])
		if kwargs['oque'] == "pesagem":
			obj = Pesagem.objects.get(id_pesagem=kwargs['id_doque'])
		elif kwargs['oque'] == "compra":
			obj = Compra.objects.get(id_compra=kwargs['id_doque'])
		else:
			return HttpResponseRedirect(comanda.get_absolute_url())
		obj.deletado = 1
		obj.save()
		return HttpResponseRedirect(comanda.get_absolute_url())

class NovaComanda(View):
	def get(self, request, *args, **kwargs):
		form_comanda = ComandaForm1(initial={'entradas_infantil':0})
		context = {
			'title':"Nova Comanda",
			'form':form_comanda,
		}
		return render(request, 'nova_comanda.html', context)

	def post(self, request, *args, **kwargs):
		
		form = ComandaForm1(request.POST)
		if form.is_valid():
			inst = request.POST.copy()
			inst['valor'] = Decimal(Comanda.valores_entrada[float(inst['entrada_adulto'])]) + (
					Decimal(Comanda.valores_entrada['entrada_infantil']) * Decimal(inst['entradas_infantil'])
				)
			form2 = ComandaForm2(inst)
			if form2.is_valid():
				try:
					nova_comanda = form2.save()
				except IntegrityError:
					form_comanda = ComandaForm1()
					messages.error(request, 'Num Comanda ja aberto.')
					context = {
						'title':"Nova Comanda",
						'form':form_comanda,
					}
					return render(request, 'nova_comanda.html', context)
		comanda = Comanda.objects.get(id_comanda=nova_comanda.id_comanda)
		return HttpResponseRedirect(comanda.get_absolute_url())

