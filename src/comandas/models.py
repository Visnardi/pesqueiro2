from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse

from clientes.models import Cliente
# Create your models here.

class Comanda(models.Model):

	entrada_porKG = 1
	entrada_esportiva = 2

	escolhas_entrada = (
			(entrada_porKG, 'Por Kg'),
			(entrada_esportiva, 'Esportiva'),
		)

	tipos_entrada = {
		1:"Por Kg",
		2:"Esportiva",
	}

	valores_entrada = {
		'entrada_infantil':8.0,
		entrada_porKG:15.0,
		entrada_esportiva:35.0,
	}

	id_comanda = models.AutoField(primary_key=True)
	data_comanda = models.DateTimeField(auto_now=True, auto_now_add=False)
	num_comanda = models.IntegerField()
	entrada_adulto = models.PositiveIntegerField(choices=escolhas_entrada)
	entradas_infantil = models.PositiveIntegerField()
	id_cliente = models.ForeignKey(Cliente)
	valor = models.DecimalField(max_digits=6, decimal_places=2)
	status = models.BooleanField(default=1)

	def __unicode__(self):
		return str(self.num_comanda) + ' - Cliente: ' + str(self.id_cliente)

	def __str__(self):
		return str(self.num_comanda) + ' - Cliente: ' + str(self.id_cliente)

	def get_absolute_url(self):
		return reverse('detalhe', kwargs={'id_comanda':self.id_comanda})

	def computa_entradas(self):
		tipo = self.tipos_entrada[self.entrada_adulto]
		valor_adulto = self.valores_entrada[self.entrada_adulto]
		valor_infantil = self.valores_entrada['entrada_infantil'] * self.entradas_infantil
		subtotal = valor_adulto + valor_infantil

		return tipo, valor_adulto, valor_infantil, subtotal

	def encerra_comanda(self):
		self.status = 0
		self.save()