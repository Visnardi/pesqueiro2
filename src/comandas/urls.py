from django.conf.urls import url
from django.contrib import admin
from .views import *
from fechamentos.views import FechaComandas, FechaComandas2

urlpatterns = [
 	url(r'^nova/$', NovaComanda.as_view(), name='nova_comanda'),
 	url(r'^fechamento/$', FechaComandas.as_view(), name='fecha_comandas'),
 	url(r'^fechamento/2$', FechaComandas2.as_view(), name='fecha_comandas2'),
    url(r'^(?P<quais>\D+)$', ListaComandas.as_view(), name='lista'),
    url(r'^detalhe/(?P<id_comanda>\d+)/$', DetalheComanda.as_view(), name='detalhe'),
    url(r'^detalhe/(?P<id_comanda>\d+)/del/(?P<oque>\D+)/(?P<id_doque>\d+)/$', deleta.as_view(), name='deleta'),
]