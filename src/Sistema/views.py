from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import DecimalField, Sum, Count
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import View

from comandas.models import Comanda
from compras.models import Compra
from pesagens.models import Pesagem
from produtos.models import Produto
from fechamentos.models import Pagamento
from fechamentos.views import limpa_pagamentos

import datetime
from decimal import *

class Home(View):
	def get(self, request, *args, **kwargs):
		context = {}
		return render(request, 'home.html', context)

class RelatorioComandas(View):
	def get(self, request, *args, **kwargs):
		context = {
			
		}
		return render(request, 'relatorio_diario2.html', context)

	def post(self, request, *args, **kwargs):
		if request.POST['from'] == "" or request.POST['to'] == "":
			messages.error(request, 'Preencher ambas as datas.')
			return HttpResponseRedirect(reverse('relatorio_comandas'))
		data_inicio, data_fim = request.POST['from'].split("/"), request.POST['to'].split("/")
		inicio = datetime.date(int(data_inicio[0]), int(data_inicio[1]), int(data_inicio[2]))
		fim = datetime.date(int(data_fim[0]), int(data_fim[1]), int(data_fim[2]))
		comandas = Comanda.objects.filter(data_comanda__date__range=(inicio, fim)).filter(status=0)
		pesagens = Pesagem.objects.filter(id_comanda__in=comandas).filter(deletado=0)
		compras = Compra.objects.filter(id_comanda__in=comandas).filter(deletado=0)
		pagamentos = Pagamento.objects.filter(data_pagamento__date__range=(inicio, fim))
		
		peixes1 = pesagens.values('peixe').order_by('peixe').annotate(kg=Sum('peso'))
		peixes2 = {}
		total_peixes_kg, total_peixes_rs = 0, 0
		for i, k in enumerate(peixes1):
			valor_1 = round(Decimal(Pesagem.valores_peixes[k['peixe']]) * k['kg'], 2)
			peixes2[i + 1] = {
						'peixe':Pesagem.escolhas_peixe[k['peixe'] - 1][1],
						'peso':k['kg'],
						'valor':valor_1,
					}
			total_peixes_rs += valor_1
			total_peixes_kg += k['kg']
		
		limpesas1 = pesagens.values('limpesa').order_by('limpesa').annotate(kg=Sum('peso'))
		limpesas2 = {}
		total_limpesa, total_limpesa_kg = 0, 0
		for i, k in enumerate(limpesas1):
			valor_1 = round(Decimal(Pesagem.valores_limpesa[k['limpesa']]) * k['kg'], 2)
			limpesa = Pesagem.escolhas_limpesa[k['limpesa'] - 1][1]
			limpesas2[i] = {
				'limpesa':limpesa,
				'peso':k['kg'],
				'valor':valor_1,
			}
			total_limpesa += valor_1
			if not limpesa == "Sem Limpesa":
				total_limpesa_kg += k['kg']

		entradas_adulto1 = comandas.values('entrada_adulto').order_by(
			'entrada_adulto').annotate(qtd_tipo_entrada=Count('entrada_adulto'))
		entradas_adulto2 = {}
		total_entradas = 0
		for i, k in enumerate(entradas_adulto1):
			valor_1 = round(Decimal(Comanda.valores_entrada[k['entrada_adulto']]) * k['qtd_tipo_entrada'], 2)
			entradas_adulto2[i + 1] = {
				'tipo_entrada':Comanda.tipos_entrada[k['entrada_adulto']],
				'qtd':k['qtd_tipo_entrada'],
				'valor':valor_1,
			}
			total_entradas += valor_1
		
		entradas_infantil = 0
		for comanda in comandas:
			entradas_infantil += comanda.entradas_infantil
		entradas_infantil_rs = round(entradas_infantil * Decimal(Comanda.valores_entrada['entrada_infantil']), 2)
		total_entradas += entradas_infantil_rs


		total_produtos = 0
		produtos1 = compras.values('id_produto').order_by('id_produto').annotate(qtd_total=Sum('qtd'))
		produtos2 = {}
		for i, k in enumerate(produtos1):
			valor_1 = Produto.objects.get(id_produto=k['id_produto']).valor *  k['qtd_total']
			produtos2[i + 1] = {
				'produto':Produto.objects.get(id_produto=k['id_produto']).nome,
				'qtd':k['qtd_total'],
				'valor':valor_1,
			}
			total_produtos += valor_1

		total = total_produtos + total_entradas + total_limpesa + total_peixes_rs


		pagamentos1 = pagamentos.values('tipo_pagamento').order_by('tipo_pagamento').annotate(valor_total=Sum('valor'))
		pagamentos2 = {}
		for i, k in enumerate(pagamentos1):
			pagamentos2[i + 1] = {
				'tipo_pgto':Pagamento.tipos_pagamento[k['tipo_pagamento']],
				'valor':k['valor_total'],
			}

		context = {
			'data_inicio':inicio.strftime("%d/%m/%y"),
			'data_fim':fim.strftime("%d/%m/%y"),
			'total_peixes_rs':total_peixes_rs,
			'total_peixes_kg':total_peixes_kg,
			'peixes':peixes2,
			'produtos':produtos2,
			'total_produtos':total_produtos,
			'total_limpesa':total_limpesa,
			'total_limpesa_kg':total_limpesa_kg,
			'limpesas':limpesas2,
			'entradas_adulto2':entradas_adulto2,
			'entradas_infantil':entradas_infantil,
			'entradas_infantil_rs':entradas_infantil_rs,
			'total_entradas':total_entradas,
			'total':total,
			'pgtos':pagamentos2,

		}
		return render(request, 'relatorio_diario1.html', context)