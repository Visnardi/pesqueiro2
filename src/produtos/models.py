from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Produto(models.Model):

	id_produto = models.AutoField(primary_key=True)
	nome = models.CharField(max_length=40)
	valor = models.DecimalField(max_digits=6, decimal_places=2)
	inventariavel = models.BooleanField(default=1)
	estoque = models.IntegerField()
	status = models.BooleanField(default=1)

	def __unicode__(self):
		return str(self.nome)

	def __str__(self):
		return str(self.nome)