from django.conf.urls import url
from django.contrib import admin
from .views import *

urlpatterns = [
    url(r'^novo/$', CadastrarCliente.as_view(), name='novo_cliente'),
    url(r'^lista/$', ClientesLista.as_view(), name='lista_clientes'),
    url(r'^detalhe/(?P<id_cliente>\d+)$', ClientesDetalhe.as_view(), name='clientes_detalhe'),
]