from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.db import models

# Create your models here.

class Cliente(models.Model):

	id_cliente = models.AutoField(primary_key=True)
	nome = models.CharField(max_length=40)
	sobrenome = models.CharField(max_length=80)
	rg = models.CharField(max_length=20)
	foto = models.ImageField(upload_to='imagens/clientes/', null=True, blank=True)
	email = models.EmailField()
	status = models.IntegerField(default=0)
	cod_comandas_furo = models.CharField(max_length=20, null=True, blank=True)

	def get_absolute_url(self):
		return reverse('clientes_detalhe', kwargs={'id_cliente':self.id_cliente})


	def __unicode__(self):
		return str(self.id_cliente) + ' - ' + str(self.nome) + ' ' + str(self.sobrenome)

	def __str__(self):
		return str(self.nome) + ' ' + str(self.sobrenome) + ' - ' + str(self.id_cliente)