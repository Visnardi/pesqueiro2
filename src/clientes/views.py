from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.files import File 
from django.core.urlresolvers import reverse
from django.views.generic import View
from .models import Cliente
from .forms import ClienteForm, ClienteForm2
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from Sistema.settings import MEDIA_ROOT
from os import path as PATH

from base64 import b64decode

# Create your views here.

class CadastrarCliente(View):

	def get(self, request, *args, **kwargs):

		form = ClienteForm()
		context = {
			'title':"Novo Cliente",
			'form': form,
		}
		return render(request, 'novo_cliente.html', context)

	def post(self, request, *args, **kwargs):
		form = ClienteForm(request.POST or None)
		if form.is_valid():
			instance = form.save(commit=False)
			instance.save()
			#instance.id_cliente
			if request.POST['boolFoto']:
				img_data = b64decode(request.POST['foto'][-(len(request.POST['foto']) - 24):])
				fh = open(PATH.join(MEDIA_ROOT, 'imagens', 'clientes', 'img.jpeg'), 'w+b')
				fh.write(img_data)
				instance.foto = File(fh)
				instance.save()
				fh.close()
			if "continua" in request.POST:
				return HttpResponseRedirect(reverse('novo_cliente'))
			elif "voltar" in request.POST:
				return HttpResponseRedirect(reverse('nova_comanda'))
		else:
			return HttpResponseRedirect(reverse('novo_cliente'))

class ClientesLista(View):
	def get(self, request, *args, **kwargs):
		lista_clientes = Cliente.objects.all()

		paginator = Paginator(lista_clientes, 25) # Show 25 contacts per page
		page = request.GET.get('page')
		try:
			clientes = paginator.page(page)
		except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			clientes = paginator.page(1)
		except EmptyPage:
			# If page is out of range (e.g. 9999), deliver last page of results.
			clientes = paginator.page(paginator.num_pages)


		context = {
			'clientes':clientes,
			}
		return render(request, 'lista_clientes.html', context)

class ClientesDetalhe(View):
	def get(self, request, *args, **kwargs):
		cliente = Cliente.objects.get(id_cliente=kwargs['id_cliente'])
		context = {
			'cliente':cliente,
		}

		return render(request, 'detalhe_cliente.html' , context)