from django import forms
from .models import Cliente

class ClienteForm(forms.ModelForm):
	class Meta:
		model = Cliente
		fields = [
			'nome',
			'sobrenome',
			'rg',
			'email',
		]

class ClienteForm2(forms.ModelForm):
	class Meta:
		model = Cliente
		fields = [
			'nome',
			'sobrenome',
			'rg',
			'email',
			'foto',
		]