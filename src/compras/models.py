from __future__ import unicode_literals

from django.db import models
from comandas.models import Comanda
from produtos.models import Produto
# Create your models here.

class Compra(models.Model):

	id_compra = models.AutoField(primary_key=True)
	data_compra = models.DateTimeField(auto_now=False, auto_now_add=True)
	id_comanda = models.ForeignKey(Comanda)
	id_produto = models.ForeignKey(Produto)
	qtd = models.PositiveIntegerField()
	valor = models.DecimalField(max_digits=8, decimal_places=2)
	deletado = models.BooleanField(default=0)
	data_deletado = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __unicode__(self):
		return str(self.id_compra) + ' ' + str(self.id_produto)

	def __str__(self):
		return str(self.id_compra) + ' ' + str(self.id_produto)