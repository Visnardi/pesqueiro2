from django import forms
from .models import Compra

class CompraForm1(forms.ModelForm):
	class Meta:
		model = Compra
		fields = [
			'id_produto',
			'qtd',
		]
	def __init__(self, *args, **kwargs):
		super(CompraForm1, self).__init__(*args, **kwargs)
		self.fields['qtd'].widget.attrs['style'] = "width:100px"

class CompraForm2(forms.ModelForm):
	class Meta:
		model = Compra
		fields = [
			'id_comanda',
			'id_produto',
			'qtd',
			'valor',
		]