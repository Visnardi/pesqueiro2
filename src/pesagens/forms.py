from django import forms
from .models import Pesagem

class PesagemForm1(forms.ModelForm):
	class Meta:
		model = Pesagem
		fields = [
			'peixe',
			'peso',
			'limpesa',
		]
	def __init__(self, *args, **kwargs):
		super(PesagemForm1, self).__init__(*args, **kwargs)
		self.fields['peso'].widget.attrs['style'] = "width:100px"

class PesagemForm2(forms.ModelForm):
	class Meta:
		model = Pesagem
		fields = [
			'peso',
			'id_comanda',
			'peixe',
			'limpesa',
			'valor'
		]