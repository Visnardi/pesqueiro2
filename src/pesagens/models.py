from __future__ import unicode_literals
from decimal import * 

from django.db import models
from django import forms
from comandas.models import Comanda
# Create your models here.


class Pesagem(models.Model):
	"""
	Manter opcoes/valores em ordem!
	opcao limpesa_nenhuma = 1 obrigatorio
	"""
	limpesa_nenhuma = 1
	limpesa_simples = 2
	limpesa_porquinho = 3
	limpesa_file = 4

	escolhas_limpesa = (
			(limpesa_nenhuma, "Sem Limpesa"),
			(limpesa_simples, "Simples"),
			(limpesa_porquinho, "Porquinho"),
			(limpesa_file, "File"),
		)

	valores_limpesa = {
		limpesa_nenhuma:0,
		limpesa_simples:1.0,
		limpesa_porquinho:1.5,
		limpesa_file:1.8,
	}

	"""Seguir padrao para incluir novos peixes
			declarar peixe_PEIXE = INTEIRO
			incluir (peixe_PEIXE, Descricao) em escolhas_peixe
			incluir peixe_PEIXE:PRECO (com .0 se inteiro) em valores_peixe
	"""

	peixe_tilapia = 1
	peixe_catfish = 2
	peixe_pacu = 3
	peixe_dourado = 4
	peixe_pintado = 5
	peixe_carpa = 6
	peixe_traira = 7
	peixe_piau = 8


	escolhas_peixe = (
			(peixe_tilapia, "Tilapia"),
			(peixe_catfish, "Catfish"),
			(peixe_pacu, "Pacu"),
			(peixe_dourado, "Dourado"),
			(peixe_pintado, "Pintado"),
			(peixe_carpa, "Carpa"),
			(peixe_traira, "Traira"),
			(peixe_piau, "Piau"),
		)

	valores_peixes = {
		peixe_tilapia:14.0,
		peixe_catfish:14.0,
		peixe_pacu:14.0,
		peixe_dourado:30.0,
		peixe_pintado:30.0,
		peixe_carpa:14.0,
		peixe_traira:14.0,
		peixe_piau:14.0,
	}

	id_pesagem = models.AutoField(primary_key = True)
	data_pesagem = models.DateTimeField(auto_now=False, auto_now_add=True)
	peso = models.DecimalField(max_digits=8, decimal_places=4)
	id_comanda = models.ForeignKey(Comanda)
	peixe = models.PositiveIntegerField(choices=escolhas_peixe)
	limpesa = models.PositiveIntegerField(choices=escolhas_limpesa)
	valor = models.DecimalField(max_digits=8, decimal_places=2)
	deletado = models.BooleanField(default=0)
	data_deletado = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __unicode__(self):
		return str(self.id_pesagem) + ' ' + str(self.peixe)

	def __str__(self):
		return str(self.id_pesagem) + ' ' + str(self.peixe)

	def computa_pesagem(self):
		tipo_peixe = self.escolhas_peixe[self.peixe - 1][1]
		tipo_limpesa = self.escolhas_limpesa[self.limpesa - 1][1]
		valor_pesagem = (Decimal(self.valores_peixes[self.peixe]) + Decimal(self.valores_limpesa[self.limpesa])) * Decimal(self.peso)
		return tipo_peixe, tipo_limpesa, valor_pesagem